@extends('designer.layouts.default')
@section('content')
    <div id="page_content">
        <div id="page_content_inner">
            @if (session('successmsg'))
                <div class="uk-alert uk-alert-success">
                    <a class="uk-close uk-alert-close"></a>
                    {{ session('successmsg') }}
                </div>
            @endif
            <h3 class="heading_b uk-margin-bottom">Datatables</h3>
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <table id="dt_default" class="uk-table" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Keterangan</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($trees as $value)
                            <tr>
                                <td>{{$value->nama}}</td>
                                <td>{{$value->keterangan}}</td>
                                <td class="uk-text-center">
                                    <a href="{{url('tree/tree/add?i='.$value->id)}}"><i class="md-icon material-icons"></i></a>
                                    <a href="{{url('tree/tree/del?i='.$value->id)}}"><i class="md-icon material-icons"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="md-fab-wrapper">
        <a class="md-fab md-fab-accent" href="{{url('tree/trees/add')}}">
            <i class="material-icons">&#xE145;</i>
        </a>
    </div>
@endsection
@push('up-css')
    <link rel="stylesheet"
          href="{{asset('assets/designer/bower_components/kendo-ui/styles/kendo.common-material.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/designer/bower_components/kendo-ui/styles/kendo.material.min.css')}}"
          id="kendoCSS"/>
@endpush
@push('css')
    <link type="text/css" rel="stylesheet" href="{{asset('assets/designer/bower_components/jsmind/jsmind.css')}}"/>
    <style type="text/css">
        #jsmind_container {
            float: left;
            width: 1269px;
            height: 600px;
            border: solid 1px #ccc;
            background: #f4f4f4;
        }
    </style>
@endpush
@push('script')
    <script type="text/javascript" src="{{asset('assets/designer/assets/js/kendoui_custom.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/designer/assets/js/pages/kendoui.min.js')}}"></script>
    <script src="{{asset('assets/designer/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/designer/bower_components/datatables-buttons/js/dataTables.buttons.js')}}"></script>
    <script src="{{asset('assets/designer/assets/js/custom/datatables/buttons.uikit.js')}}"></script>
    <script src="{{asset('assets/designer/bower_components/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{asset('assets/designer/bower_components/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/designer/bower_components/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/designer/bower_components/datatables-buttons/js/buttons.colVis.js')}}"></script>
    <script src="{{asset('assets/designer/bower_components/datatables-buttons/js/buttons.html5.js')}}"></script>
    <script src="{{asset('assets/designer/bower_components/datatables-buttons/js/buttons.print.js')}}"></script>
    <script src="{{asset('assets/designer/assets/js/custom/datatables/datatables.uikit.min.js')}}"></script>
    <script>
        $(function () {
            this_form.init_dt()
        }), this_form = {
            init_dt: function () {
                var t = $("#dt_default");
                t.length && t.DataTable()
            }
        };
    </script>
@endpush
