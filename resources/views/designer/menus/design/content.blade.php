@extends('designer.layouts.default')
@section('content')
<div id="page_content">
  <div id="page_content_inner">
    <div class="md-card">
      <div class="md-card-content">
        <div class="uk-grid" data-uk-grid-margin>
          <div class="uk-width-large-1-1">
              <ul id="kUI_menu">
                <li>
                    File
                    <ul>
                        <li onclick="open_json();">Open Example</li>
                        <li onclick="save_file();">Save File</li>
                        <li>
                          Open
                          <ul>
                              <li>
                                  <input id="file_input" class="file_input" type="file"/>Browse
                              </li>
                              <li onclick="open_file();">Load</li>
                          </ul>
                        </li>
                    </ul>
                </li>
                <li>
                    Toggle
                    <ul>
                        <li onclick="open_json();">Example</li>
                        <li onclick="open_ajax();">Remote</li>
                        <li onclick="prompt_info('see 6.Multi Format');">Remote</li>
                    </ul>
                </li>
                <li>
                    Edit
                    <ul>
                        <li onclick="add_node();">Add Node</li>
                        <li onclick="show_selected();">Show Info Selected</li>
                    </ul>
                </li>
              </ul>
          </div>
          <div class="uk-width-1-1">
            <div class="md-card">
              <div id="jsmind_container">
              </div>
              <div style="display:none">
                <input class="file" type="file" id="image-chooser" accept="image/*"/>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="uk-modal" id="myModalParent">
    <div class="uk-modal-dialog">
        <div class="uk-modal-header">
            <h3 class="uk-modal-title">Options <i class="material-icons" data-uk-tooltip="{pos:'top'}" title="headline tooltip">&#xE8FD;</i></h3>
        </div>
        <div class="uk-grid" data-uk-grid-margin>
          <div class="uk-width-medium-1-1">
            <form>
              <div class="uk-form-row">
                <label>Parent :</label>
                <input type="text" class="md-input label-fixed" id="parentParent" readonly>
              </div>
              <div class="uk-form-row">
                <label>Name :</label>
                <input type="text" class="md-input label-fixed" id="topicParent">
              </div>
              <div class="uk-form-row">
                <label>Ip Adress :</label>
                <input type="text" class="md-input label-fixed" id="ipaddressParent">
              </div>
              <div class="uk-form-row">
                <label>Percent Upload :</label>
                <input type="text" class="md-input label-fixed" id="percentuploadParent" readonly>
              </div>
              <div class="uk-form-row">
                <label>Max Limit Upload :</label>
                <input type="text" class="md-input label-fixed" id="bandwidthuploadParent">
              </div>
              <div class="uk-form-row">
                <label>Percent Download :</label>
                <input type="text" class="md-input label-fixed" id="percentdownloadParent" readonly>
              </div>
              <div class="uk-form-row">
                <label>Max Limit Download :</label>
                <input type="text" class="md-input label-fixed" id="bandwidthdownloadParent">
              </div>
              <div class="uk-form-row">
                <label>Priority :</label>
                <input type="text" class="md-input label-fixed" id="priorityParent">
              </div>
            </form>
          </div>
        </div>
        <div class="uk-modal-footer uk-text-right">
            <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
            <button type="button" id="savechangeParent" class="md-btn md-btn-success">Save</button>
            <button type="button" id="executeChange" class="md-btn md-btn-warning">Execute</button>
        </div>
    </div>
</div>

<div class="uk-modal" id="myModal">
    <div class="uk-modal-dialog">
        <div class="uk-modal-header">
            <h3 class="uk-modal-title">Options <i class="material-icons" data-uk-tooltip="{pos:'top'}" title="headline tooltip">&#xE8FD;</i></h3>
        </div>
        <div class="uk-grid" data-uk-grid-margin>
          <div class="uk-width-medium-1-1">
            <form>
              <div class="uk-form-row">
                <label>Parent :</label>
                <input type="text" class="md-input label-fixed" id="parent" readonly>
              </div>
              <div class="uk-form-row">
                <label>Name :</label>
                <input type="text" class="md-input label-fixed" id="topic">
              </div>
              <div class="uk-form-row">
                <label>Ip Adress :</label>
                <input type="text" class="md-input label-fixed" id="ipaddress">
              </div>
              <div class="uk-form-row">
                <label>Percent Upload :</label>
                <input type="text" class="md-input label-fixed" id="percentupload" readonly>
              </div>
              <div class="uk-form-row">
                <label>Max Limit Upload :</label>
                <input type="text" class="md-input label-fixed" id="maxlimitupload">
              </div>
              <div class="uk-form-row">
                <label>Percent Download :</label>
                <input type="text" class="md-input label-fixed" id="percentdownload" readonly>
              </div>
              <div class="uk-form-row">
                <label>Max Limit Download :</label>
                <input type="text" class="md-input label-fixed" id="maxlimitdownload">
              </div>
              <div class="uk-form-row">
                <label>Priority :</label>
                <input type="text" class="md-input label-fixed" id="priority">
              </div>
            </form>
          </div>
        </div>
        <div class="uk-modal-footer uk-text-right">
            <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
            <button type="button" id="savechange" class="md-btn md-btn-success">Save</button>
        </div>
    </div>
</div>
@endsection
@push('up-css')
<link rel="stylesheet" href="{{asset('assets/designer/bower_components/kendo-ui/styles/kendo.common-material.min.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/designer/bower_components/kendo-ui/styles/kendo.material.min.css')}}" id="kendoCSS"/>
@endpush
@push('css')
<link type="text/css" rel="stylesheet" href="{{asset('assets/designer/bower_components/jsmind/jsmind.css')}}" />
<style type="text/css">
    #jsmind_container{
        float:left;
        width:1269px;
        height:600px;
        border:solid 1px #ccc;
        background:#f4f4f4;
    }
</style>
@endpush
@push('script')
<script type="text/javascript" src="{{asset('assets/designer/assets/js/kendoui_custom.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/designer/assets/js/pages/kendoui.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/designer/bower_components/jsmind/jsmind.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/designer/bower_components/jsmind/jsmind.draggable.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/designer/bower_components/jsmind/jsmind.screenshot.js')}}"></script>
<script type="text/javascript">
    var _jm = null;
    function open_empty(){
        var options = {
            container:'jsmind_container',
            theme:'greensea',
            editable:true
        }
        _jm = jsMind.show(options);
    }

    function open_json(){
        var mind = {
            "meta":{
                "name":"AutoQueue",
                "author":"daniel@gmail.com",
                "version":"0.2"
            },
            "format":"node_tree",
            "data":{"id":"root","topic":"jsMind","children":[
                {"id":"easy","topic":"Easy","direction":"left","children":[
                    {"id":"easy1","topic":"Easy to show"},
                    {"id":"easy2","topic":"Easy to edit"},
                    {"id":"easy3","topic":"Easy to store"},
                    {"id":"easy4","topic":"Easy to embed"},
                    {"id":"other3","background-image":"ant.png", "width": "100", "height": "100"}
                ]},
                {"id":"open","topic":"Open Source","direction":"right","children":[
                    {"id":"open1","topic":"on GitHub", "background-color":"#eee", "foreground-color":"blue"},
                    {"id":"open2","topic":"BSD License"}
                ]},
                {"id":"powerful","topic":"Powerful","direction":"right","children":[
                    {"id":"powerful1","topic":"Base on Javascript"},
                    {"id":"powerful2","topic":"Base on HTML5"},
                    {"id":"powerful3","topic":"Depends on you"}
                ]},
                {"id":"other","topic":"test node","direction":"left","children":[
                    {"id":"other1","topic":"I'm from local variable"},
                    {"id":"other2","topic":"I can do everything"}
                ]}
            ]}
        }
        _jm.show(mind);
    }

    function open_ajax(){
        var mind_url = 'data_example.json';
        jsMind.util.ajax.get(mind_url,function(mind){
            _jm.show(mind);
        });
    }

    function screen_shot(){
        _jm.screenshot.shootDownload();
    }

    function show_data(){
        var mind_data = _jm.get_data();
        var mind_string = jsMind.util.json.json2string(mind_data);
        prompt_info(mind_string);
    }

    function save_file(){
        var mind_data = _jm.get_data();
        var mind_name = mind_data.meta.name;
        var mind_str = jsMind.util.json.json2string(mind_data);
        jsMind.util.file.save(mind_str,'text/jsmind',mind_name+'.jm');
    }

    function open_file(){
        var file_input = document.getElementById('file_input');
        var files = file_input.files;
        if(files.length > 0){
            var file_data = files[0];
            jsMind.util.file.read(file_data,function(jsmind_data, jsmind_name){
                var mind = jsMind.util.json.string2json(jsmind_data);
                if(!!mind){
                    _jm.show(mind);
                }else{
                    prompt_info('can not open this file as mindmap');
                }
            });
        }else{
            prompt_info('please choose a file first')
        }
    }

    function select_node(){
        var nodeid = 'other';
        _jm.select_node(nodeid);
    }

    function show_selected(){
        var selected_node = _jm.get_selected_node();
        if(!!selected_node){
          console.log(_jm.get_selected_node());
          var selected =  _jm.get_selected_node();
          if (selected.isroot){
            $('#topicParent').val(selected.topic);
            $('#bandwidthuploadParent').val(selected.data.maxupload);
            $('#bandwidthdownloadParent').val(selected.data.maxdownload);
            // $('#myModalParent').modal('show');
            var modal = UIkit.modal("#myModalParent");
            if ( modal.isActive() ) {
                modal.hide();
            } else {
                modal.show();
            }
          }
          else {
            $('#topic').val(selected.topic);
            $('#parent').val(selected.parent.topic);
            $('#priority').val(selected.data.priority);
            $('#ipaddress').val(selected.data.ipaddress);
            $('#percentupload').val(selected.data.percentupload);
            $('#percentdownload').val(selected.data.percentdownload);
            $('#maxlimitupload').val(selected.data.maxupload);
            $('#maxlimitdownload').val(selected.data.maxdownload);
            // $('#myModal').modal('show');
            var modal = UIkit.modal("#myModal");
            if ( modal.isActive() ) {
                modal.hide();
            } else {
                modal.show();
            }
          }
        }else{
            prompt_info('nothing');
        }
    }
    $('#savechange').on('click',function(){
      var selected_id = get_selected_nodeid();
      if(!selected_id){prompt_info('please select a node first.');return;}
      var selected_node = _jm.get_selected_node();
      selected_node.topic = $('#topic').val();
      selected_node.data.ipaddress = $('#ipaddress').val();
      selected_node.data.priority = $('#priority').val();
      selected_node.data.maxupload = $('#maxlimitupload').val();
      // selected_node.data.percent = ($('#maxlimit').val()/selected_node.parent.data.max)*100;
      selected_node.data.percentupload = ($('#maxlimitupload').val()/$IspMaxUpload)*100;
      selected_node.data.maxdownload = $('#maxlimitdownload').val();
      selected_node.data.percentdownload = ($('#maxlimitdownload').val()/$IspMaxDownload)*100;
      _jm.update_node(selected_id,selected_node.topic,selected_node);
      // $('#myModal').modal('hide');
      var modal = UIkit.modal("#myModal");
      if ( modal.isActive() ) {
          modal.hide();
      } else {
          modal.show();
      }
    });

    $IspMaxDownload = 0;
    $IspMaxUpload = 0;
    $('#savechangeParent').on('click',function(){
      var selected_id = get_selected_nodeid();
      if(!selected_id){prompt_info('please select a node first.');return;}
      var selected_node = _jm.get_selected_node();
      selected_node.topic = $('#topicParent').val();
      selected_node.data.ipaddress = "0"
      selected_node.data.priority = "0";
      selected_node.data.maxupload = $('#bandwidthuploadParent').val();
      selected_node.data.maxdownload = $('#bandwidthdownloadParent').val();
      selected_node.data.percentupload = "0";
      selected_node.data.percentdownload = "0";
      $IspMaxUpload = selected_node.data.maxupload;
      $IspMaxDownload = selected_node.data.maxdownload;
      _jm.update_node(selected_id,selected_node.topic,selected_node);
      // $('#myModalParent').modal('hide');
      var modal = UIkit.modal("#myModalParent");
      if ( modal.isActive() ) {
          modal.hide();
      } else {
          modal.show();
      }
    });
    $('#executeChange').on('click',function(){
        var mind_data = _jm.get_data('node_array');
        console.log(mind_data);
        $.each(mind_data.data, function( index, value ) {
          if (index!=0)
          {
            var selected_node =_jm.get_node(value.id);
            selected_node.data.maxupload = ($IspMaxUpload/100)*selected_node.data.percentupload;
            selected_node.data.maxdownload = ($IspMaxDownload/100)*selected_node.data.percentdownload;
            // console.log(_jm.get_node(value.id));
            _jm.update_node(value.id,selected_node.topic,selected_node);
            // $('#myModalParent').modal('hide');
            var modal = UIkit.modal("#myModalParent");
            if ( modal.isActive() ) {
                modal.hide();
            } else {
                modal.show();
            }
          }
        });
    });
    function get_selected_nodeid(){
        var selected_node = _jm.get_selected_node();
        if(!!selected_node){
            return selected_node.id;
        }else{
            return null;
        }
    }

    function add_node(){
        var selected_node = _jm.get_selected_node(); // as parent of new node
        if(!selected_node){prompt_info('please select a node first.');return;}
        var nodeid = jsMind.util.uuid.newid();
        var topic = '* Node_'+nodeid.substr(0,5)+' *';
        var data = new Object();
        data.priority = "0";
        data.ipaddress = "0";
        data.maxupload = "0";
        data.maxdownload = "0";
        data.percentupload = "0";
        data.percentdownload = "0";
        var node = _jm.add_node(selected_node, nodeid, topic,data);
    }

    var imageChooser = document.getElementById('image-chooser');

    imageChooser.addEventListener('change', function (event) {
        // Read file here.
        var reader = new FileReader();
        reader.onloadend = (function () {
            var selected_node = _jm.get_selected_node();
            var nodeid = jsMind.util.uuid.newid();
            var topic = undefined;
            var data = {
                "background-image": reader.result,
                "width": "100",
                "height": "100"};
            var node = _jm.add_node(selected_node, nodeid, topic, data);
            //var node = _jm.add_image_node(selected_node, nodeid, reader.result, 100, 100);
        //add_image_node:function(parent_node, nodeid, image, width, height, data, idx, direction, expanded){
        });

        var file = imageChooser.files[0];
        if (file) {
            reader.readAsDataURL(file);
        };

    }, false);

    function add_image_node(){
        var selected_node = _jm.get_selected_node(); // as parent of new node
        if(!selected_node){
            prompt_info('please select a node first.');
            return;
        }

        imageChooser.focus();
        imageChooser.click();
    }

    function modify_node(){
        var selected_id = get_selected_nodeid();
        if(!selected_id){prompt_info('please select a node first.');return;}

        // modify the topic
        _jm.update_node(selected_id, '--- modified ---');
    }

    function move_to_first(){
        var selected_id = get_selected_nodeid();
        if(!selected_id){prompt_info('please select a node first.');return;}

        _jm.move_node(selected_id,'_first_');
    }

    function move_to_last(){
        var selected_id = get_selected_nodeid();
        if(!selected_id){prompt_info('please select a node first.');return;}

        _jm.move_node(selected_id,'_last_');
    }

    function move_node(){
        // move a node before another
        _jm.move_node('other','open');
    }

    function remove_node(){
        var selected_id = get_selected_nodeid();
        if(!selected_id){prompt_info('please select a node first.');return;}

        _jm.remove_node(selected_id);
    }

    function change_text_font(){
        var selected_id = get_selected_nodeid();
        if(!selected_id){prompt_info('please select a node first.');return;}

        _jm.set_node_font_style(selected_id, 28);
    }

    function change_text_color(){
        var selected_id = get_selected_nodeid();
        if(!selected_id){prompt_info('please select a node first.');return;}

        _jm.set_node_color(selected_id, null, '#000');
    }

    function change_background_color(){
        var selected_id = get_selected_nodeid();
        if(!selected_id){prompt_info('please select a node first.');return;}

        _jm.set_node_color(selected_id, '#eee', null);
    }

    function change_background_image(){
        var selected_id = get_selected_nodeid();
        if(!selected_id){prompt_info('please select a node first.');return;}

        _jm.set_node_background_image(selected_id, 'ant.png', 100, 100);
    }

    function set_theme(theme_name){
        _jm.set_theme(theme_name);
    }

    var zoomInButton = document.getElementById("zoom-in-button");
    var zoomOutButton = document.getElementById("zoom-out-button");

    function zoomIn() {
        if (_jm.view.zoomIn()) {
            zoomOutButton.disabled = false;
        } else {
            zoomInButton.disabled = true;
        };
    };

    function zoomOut() {
        if (_jm.view.zoomOut()) {
            zoomInButton.disabled = false;
        } else {
            zoomOutButton.disabled = true;
        };
    };

    function toggle_editable(btn){
        var editable = _jm.get_editable();
        if(editable){
            _jm.disable_edit();
            btn.innerHTML = 'enable editable';
        }else{
            _jm.enable_edit();
            btn.innerHTML = 'disable editable';
        }
    }

    // this method change size of container, perpare for adjusting jsmind
    function change_container(){
        var c = document.getElementById('jsmind_container');
        c.style.width = '800px';
        c.style.height = '500px';
    }

    function resize_jsmind(){
        _jm.resize();
    }

    function expand(){
        var selected_id = get_selected_nodeid();
        if(!selected_id){prompt_info('please select a node first.');return;}

        _jm.expand_node(selected_id);
    }

    function collapse(){
        var selected_id = get_selected_nodeid();
        if(!selected_id){prompt_info('please select a node first.');return;}

        _jm.collapse_node(selected_id);
    }

    function toggle(){
        var selected_id = get_selected_nodeid();
        if(!selected_id){prompt_info('please select a node first.');return;}

        _jm.toggle_node(selected_id);
    }

    function expand_all(){
        _jm.expand_all();
    }

    function expand_to_level2(){
        _jm.expand_to_depth(2);
    }

    function expand_to_level3(){
        _jm.expand_to_depth(3);
    }

    function collapse_all(){
        _jm.collapse_all();
    }


    function get_nodearray_data(){
        var mind_data = _jm.get_data('node_array');
        var mind_string = jsMind.util.json.json2string(mind_data);
        prompt_info(mind_string);
    }

    function save_nodearray_file(){
        var mind_data = _jm.get_data('node_array');
        var mind_name = mind_data.meta.name;
        var mind_str = jsMind.util.json.json2string(mind_data);
        jsMind.util.file.save(mind_str,'text/jsmind',mind_name+'.jm');
    }

    function open_nodearray(){
        var file_input = document.getElementById('file_input_nodearray');
        var files = file_input.files;
        if(files.length > 0){
            var file_data = files[0];
            jsMind.util.file.read(file_data,function(jsmind_data, jsmind_name){
                var mind = jsMind.util.json.string2json(jsmind_data);
                if(!!mind){
                    _jm.show(mind);
                }else{
                    prompt_info('can not open this file as mindmap');
                }
            });
        }else{
            prompt_info('please choose a file first')
        }
    }

    function get_freemind_data(){
        var mind_data = _jm.get_data('freemind');
        var mind_string = jsMind.util.json.json2string(mind_data);
        alert(mind_string);
    }

    function save_freemind_file(){
        var mind_data = _jm.get_data('freemind');
        var mind_name = mind_data.meta.name || 'freemind';
        var mind_str = mind_data.data;
        jsMind.util.file.save(mind_str,'text/xml',mind_name+'.mm');
    }

    function open_freemind(){
        var file_input = document.getElementById('file_input_freemind');
        var files = file_input.files;
        if(files.length > 0){
            var file_data = files[0];
            jsMind.util.file.read(file_data, function(freemind_data, freemind_name){
                if(freemind_data){
                    var mind_name = freemind_name;
                    if(/.*\.mm$/.test(mind_name)){
                        mind_name = freemind_name.substring(0,freemind_name.length-3);
                    }
                    var mind = {
                        "meta":{
                            "name":mind_name,
                            "author":"hizzgdev@163.com",
                            "version":"1.0.1"
                        },
                        "format":"freemind",
                        "data":freemind_data
                    };
                    _jm.show(mind);
                }else{
                    prompt_info('can not open this file as mindmap');
                }
            });
        }else{
            prompt_info('please choose a file first')
        }
    }

    function prompt_info(msg){
        alert(msg);
    }

    open_empty();
</script>
@endpush
