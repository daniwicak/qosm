@extends('designer.layouts.default')
@section('content')
    <div id="page_content">
        <div id="page_content_inner"><h3 class="heading_b uk-margin-bottom">Detail @isset($data) Edit @endisset</h3>
            <div class="md-card">
                <div class="md-card-content large-padding">
                    <form id="form_validation" class="uk-form-stacked" method="post" url="{{url()->current()}}">
                        @csrf
                        @isset($data)
                            <input type="hidden" name="id" value="{{$data->id}}">
                        @endisset
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1">
                                <div class="parsley-row">
                                    <label>Nama Router<span class="req">*</span></label>
                                    <input type="text" name="nama" required class="md-input"
                                           value="@isset($data){{$data->nama}}@endisset"/>
                                </div>
                            </div>
                            <div class="uk-width-medium-1">
                                <div class="parsley-row">
                                    <label>IP Address / IP Cloud<span class="req">*</span></label>
                                    <input type="text" name="ipcloud" required class="md-input"
                                           value="@isset($data){{$data->ipcloud}}@endisset"/>
                                </div>
                            </div>
                            <div class="uk-width-medium-1">
                                <div class="parsley-row">
                                    <label>Keterangan<span class="req">*</span></label>
                                    <input type="text" name="keterangan" required class="md-input"
                                           value="@isset($data){{$data->keterangan}}@endisset"/>
                                </div>
                            </div>
                        </div>
                        <div class="uk-grid">
                            <div class="uk-width-1-1">
                                <button type="submit" class="md-btn md-btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{--    <div class="md-fab-wrapper">--}}
    {{--        <a class="md-fab md-fab-accent" href="{{url('tree/router/add')}}">--}}
    {{--            <i class="material-icons">&#xE145;</i>--}}
    {{--        </a>--}}
    {{--    </div>--}}
@endsection
@push('up-css')
    <link rel="stylesheet"
          href="{{asset('assets/designer/bower_components/kendo-ui/styles/kendo.common-material.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/designer/bower_components/kendo-ui/styles/kendo.material.min.css')}}"
          id="kendoCSS"/>
@endpush
@push('css')
    <link type="text/css" rel="stylesheet" href="{{asset('assets/designer/bower_components/jsmind/jsmind.css')}}"/>
    <style type="text/css">
        #jsmind_container {
            float: left;
            width: 1269px;
            height: 600px;
            border: solid 1px #ccc;
            background: #f4f4f4;
        }
    </style>
@endpush
@push('script')
    <script>
        altair_forms.parsley_validation_config();
    </script>
    <script src="{{asset('assets/designer/bower_components/parsleyjs/dist/parsley.min.js')}}"></script>
    <script src="{{asset('assets/designer/assets/js/pages/forms_validation.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/designer/assets/js/kendoui_custom.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/designer/assets/js/pages/kendoui.min.js')}}"></script>
    <script src="{{asset('assets/designer/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/designer/bower_components/datatables-buttons/js/dataTables.buttons.js')}}"></script>
    <script src="{{asset('assets/designer/assets/js/custom/datatables/buttons.uikit.js')}}"></script>
    <script src="{{asset('assets/designer/bower_components/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{asset('assets/designer/bower_components/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/designer/bower_components/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/designer/bower_components/datatables-buttons/js/buttons.colVis.js')}}"></script>
    <script src="{{asset('assets/designer/bower_components/datatables-buttons/js/buttons.html5.js')}}"></script>
    <script src="{{asset('assets/designer/bower_components/datatables-buttons/js/buttons.print.js')}}"></script>
    <script src="{{asset('assets/designer/assets/js/custom/datatables/datatables.uikit.min.js')}}"></script>
    <script>
        $(function () {
            this_form.init_dt()
        }), this_form = {
            init_dt: function () {
                var t = $("#dt_default");
                t.length && t.DataTable()
            }
        };
    </script>
@endpush
