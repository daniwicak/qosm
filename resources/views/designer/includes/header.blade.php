<header id="header_main">
  <div class="header_main_content">
      <nav class="uk-navbar">
          <div class="main_logo_top">
            <a href="index.html"><img src="assets/img/logo_main_white.png" alt="" height="15" width="71"/></a>
          </div>
          <a href="#" id="sidebar_secondary_toggle" class="sSwitch sSwitch_right sidebar_secondary_check">
              <span class="sSwitchIcon"></span>
          </a>
          <div class="uk-navbar-flip">
              <ul class="uk-navbar-nav user_actions">
                  <li><a href="#" id="full_screen_toggle" class="user_action_icon uk-visible-large"><i class="material-icons md-24 md-light">&#xE5D0;</i></a></li>
                  <li data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
                      <a href="#" class="user_action_image"><img class="md-user-image" src="{{asset('assets/designer/assets/img/avatars/avatar_11_tn.png')}}" alt=""/></a>
                      <div class="uk-dropdown uk-dropdown-small">
                          <ul class="uk-nav js-uk-prevent">
{{--                              <li><a href="page_user_profile.html">My profile</a></li>--}}
{{--                              <li><a href="page_settings.html">Settings</a></li>--}}
                              <li><a href="login.html">Logout</a></li>
                          </ul>
                      </div>
                  </li>
              </ul>
          </div>
      </nav>
  </div>
  <div class="header_main_search_form">
      <i class="md-icon header_main_search_close material-icons">&#xE5CD;</i>
  </div>
</header>
<div id="top_bar">
    <div class="md-top-bar">
        <ul id="menu_top" class="uk-clearfix">
            <li class="uk-hidden-small"><a href="{{url('tree')}}"><i class="material-icons">&#xE88A;</i></a></li>
            <li class="uk-hidden-small"><a href="{{url('tree/router')}}">Routers</a></li>
            <li class="uk-hidden-small"><a href="{{url('tree/trees')}}">Trees</a></li>
{{--            <li data-uk-dropdown class="uk-hidden-small">--}}
{{--                <a href="{{url('tree/router')}}"><i class="material-icons">&#xE8D2;</i><span>Routers</span></a>--}}
{{--                <div class="uk-dropdown">--}}
{{--                    <ul class="uk-nav uk-nav-dropdown">--}}
{{--                        <li><a href="forms_regular.html">Regular Elements</a></li>--}}
{{--                        <li><a href="forms_advanced.html">Advanced Elements</a></li>--}}
{{--                        <li><a href="forms_file_upload.html">File Upload</a></li>--}}
{{--                        <li><a href="forms_validation.html">Validation</a></li>--}}
{{--                        <li><a href="forms_wizard.html">Wizard</a></li>--}}
{{--                        <li class="uk-nav-header">WYSIWYG Editors</li>--}}
{{--                        <li><a href="forms_wysiwyg_ckeditor.html">CKeditor</a></li>--}}
{{--                        <li><a href="forms_wysiwyg_tinymce.html">TinyMCE</a></li>--}}
{{--                    </ul>--}}
{{--                </div>--}}
{{--            </li>--}}
        </ul>
    </div>
</div>
