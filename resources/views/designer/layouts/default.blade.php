<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no"/>
    <link rel="icon" type="image/png" href="assets/img/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="assets/img/favicon-32x32.png" sizes="32x32">
    <title>@yield('title','This') | AutoQueue</title>
    @stack('up-css')
    @include('designer.includes.meta')
    @stack('css')
</head>
<body class="top_menu">
  @include('designer.includes.header')
  @yield('content')
  @include('designer.includes.script')
  @stack('script')
</body>
</html>
