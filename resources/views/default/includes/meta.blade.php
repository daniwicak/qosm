<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="Mobland - Mobile App Landing Page Template">
<meta name="keywords" content="HTML5, bootstrap, mobile, app, landing, ios, android, responsive">
<!-- <link rel="dns-prefetch" href="//fonts.googleapis.com"> -->
<link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet">
<link rel="stylesheet" href="{{asset('assets/default/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/default/css/themify-icons.css')}}">
<link rel="stylesheet" href="{{asset('assets/default/css/owl.carousel.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/default/css/style.css')}}">
