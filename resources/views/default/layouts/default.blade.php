<!doctype html>
<html lang="en">
<head>
    <title>@yield('title','Home') | QoS Management</title>
    @include('default.includes.meta')
    @stack('css')
</head>

<body data-spy="scroll" data-target="#navbar" data-offset="30">
    @include('default.includes.header')
    @yield('content')
    @include('default.includes.footer')
    @include('default.includes.script')
    @stack('script')
</body>

</html>
