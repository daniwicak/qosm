@extends('default.layouts.default')
@section('content')

<div class="section bg-gradient">
    <div class="container">
        <div class="call-to-action">
            <small>HOME</small>
            <small>/ REGISTER</small>
            <h2>REGISTER</h2>

        </div>
    </div>
</div>

<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
              @if ($errors->any())
                  <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
              @endif
              <form method="post" action="{{url()->current()}}">
                @csrf
                <div class="form-group">
                  <label>Name</label>
                  <input type="text" class="form-control {{$errors->has('nama')?'is-invalid':''}}" name="nama" placeholder="Name">
                  @if($errors->has('nama'))
                    <div class="invalid-feedback">
                      {{$errors->first('nama')}}
                    </div>
                  @endif
                </div>
                  <div class="form-group">
                    <label>Email address</label>
                    <input type="email" class="form-control {{$errors->has('email')?'is-invalid':''}}" name="email" placeholder="Enter email" value="{{old('email','')}}">
                    @if($errors->has('email'))
                      <div class="invalid-feedback">
                        {{$errors->first('email')}}
                      </div>
                    @endif
                  </div>
                <!-- <div class="form-group">
                  <label>Phone Number</label>
                  <input type="text" class="form-control" name="phone" placeholder="Enter phone number">
                </div> -->
                <div class="form-group">
                  <label>Password</label>
                  <input type="password" class="form-control" name="password" placeholder="Password">
                </div>
                <!-- <div class="form-group">
                  <label>Confirmation Password</label>
                  <input type="password" class="form-control" name="password_confirmation" placeholder="Confirmation Password">
                </div> -->
                <button type="submit" class="btn btn-primary">Submit</button>
              </form>
            </div>
        </div>
    </div>
</div>
@endsection
