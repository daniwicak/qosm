<?php

namespace App\Http\Controllers\Tree;

use App\Http\Controllers\Controller;
use App\Models\Router;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Auth;

class RouterController extends Controller
{
    public function index()
    {
        $router = Router::get();
        return view('designer.menus.router.content',compact('router'));
    }

    public function indexDetail(Request $request)
    {
        $data = Router::find($request->input('i'));
//        return $router;
        return view('designer.menus.router.detail', compact('data'));
    }

    public function delete(Request $request)
    {
        $data = Router::find($request->input('i'));
        $data->delete();
        $message = 'Router Berhasil dihapus';
        return redirect('tree/router')->withSuccessmsg($message);
    }

    public function postDetail(Request $request)
    {
//        $data = [
//            'nama' =>$request->input('nama'),
//            'nama' =>$request->input('nama'),
//            'nama' =>$request->input('nama'),
//        ];
//        Router::create($data);
//        $input = $request->all();
//        $input['users_id'] = Auth::id();
//        Router::create($input);
        $router = new Router();
        $message = 'Router Berhasil ditambahkan';
        if ($request->has('id')){
//            edit;
            $router = Router::find($request->input('id'));
            $message = 'Router Berhasil diedit';
        }
//        baru;..
        $router->nama = $request->input('nama');
        $router->ipcloud = $request->input('ipcloud');
        $router->keterangan = $request->input('keterangan');
        $router->users_id = Auth::id();
        $router->save();
        return redirect('tree/router')->withSuccessmsg($message);
    }

}
