<?php

namespace App\Http\Controllers\Tree;

use App\Http\Controllers\Controller;
use App\Models\Router;
use App\Models\Tree;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Auth;

class TreesController extends Controller
{
    public function index()
    {
        $trees = Tree::get();
        return view('designer.menus.tree.content',compact('trees'));
    }

    public function indexDetail(Request $request)
    {
        $data = Tree::find($request->input('i'));
//        return $trees;
        return view('designer.menus.tree.detail', compact('data'));
    }

    public function delete(Request $request)
    {
        $data = Tree::find($request->input('i'));
        $data->delete();
        $message = 'Tree Berhasil dihapus';
        return redirect('tree/tree')->withSuccessmsg($message);
    }

    public function postDetail(Request $request)
    {
//        $data = [
//            'nama' =>$request->input('nama'),
//            'nama' =>$request->input('nama'),
//            'nama' =>$request->input('nama'),
//        ];
//        Router::create($data);
//        $input = $request->all();
//        $input['users_id'] = Auth::id();
//        Router::create($input);
        $trees = new Tree();
        $message = 'Tree Berhasil ditambahkan';
        if ($request->has('id')){
//            edit;
            $trees = Tree::find($request->input('id'));
            $message = 'Tree Berhasil diedit';
        }
//        baru;..
        $trees->nama = $request->input('nama');
        $trees->keterangan = $request->input('keterangan');
        $trees->routers_id = Auth::id();
        $trees->save();
        return redirect('tree/tree')->withSuccessmsg($message);
    }

}
