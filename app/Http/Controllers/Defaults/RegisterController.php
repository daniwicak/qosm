<?php
namespace App\Http\Controllers\Defaults;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
  public function index()
  {
     return view('default.menus.register.index');
  }

  public function post(Request $request)
  {
    $user = new User();
    $user->nama = $request->nama;
    $user->email = $request->email;
    $user->password = Hash::make($request->password);
    $user->aktif = 1;

    $user->save();
    // return $request->all();
    return redirect("/")->withSuccessmsg('reg suces');
    //  return view('default.menus.register.index');
  }

 
}
