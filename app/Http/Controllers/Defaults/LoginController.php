<?php
namespace App\Http\Controllers\Defaults;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
  public function index()
  {
     return view('default.menus.login.index');
  }

  public function login(Request $request)
  {
    $email=$request->email;
    $password=$request->password;
    if (Auth::attempt(['email' => $email, 'password' => $password, 'aktif' => 1])) {
      // The user is active, not suspended, and exists.
      return redirect("tree");
    } 
    return redirect("login")->withErrorsmsg('login error');
    // $user = new User();
    // $user->email = $request->email;
    // $user->password = Hash::make($request->password);

    // $user->save();
    // // return $request->all();
    // return redirect("/")->withSuccessmsg('reg suces');
    // //  return view('default.menus.register.index');
  }

  public function logout(Request $request)
  {
    Auth::logout();
    return redirect("/");
  }
 
}
