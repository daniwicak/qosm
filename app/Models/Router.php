<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 19 Jan 2020 17:24:04 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Router
 * 
 * @property int $id
 * @property string $nama
 * @property string $ipcloud
 * @property string $keterangan
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $users_id
 * 
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $trees
 *
 * @package App\Models
 */
class Router extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'users_id' => 'int'
	];

	protected $fillable = [
		'nama',
		'ipcloud',
		'keterangan',
		'users_id'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'users_id');
	}

	public function trees()
	{
		return $this->hasMany(\App\Models\Tree::class, 'routers_id');
	}
}
