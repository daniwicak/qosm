<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 19 Jan 2020 17:24:04 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Tree
 *
 * @property int $id
 * @property string $nama
 * @property string $keterangan
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $update_at
 * @property string $deleted_at
 * @property int $routers_id
 *
 * @property \App\Models\Router $router
 * @property \Illuminate\Database\Eloquent\Collection $queues
 *
 * @package App\Models
 */
class Tree extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id' => 'int',
		'routers_id' => 'int'
	];

	protected $dates = [
		'update_at'
	];

	protected $fillable = [
		'nama',
		'keterangan',
		'update_at',
		'routers_id'
	];

	public function router()
	{
		return $this->belongsTo(\App\Models\Router::class, 'routers_id');
	}

	public function queues()
	{
		return $this->hasMany(\App\Models\Queue::class, 'trees_id');
	}
}
