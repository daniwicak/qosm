<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 19 Jan 2020 17:24:04 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Queue
 * 
 * @property int $id
 * @property string $nama
 * @property string $ipaddress
 * @property string $pupload
 * @property string $mupload
 * @property string $pdownload
 * @property string $mdownload
 * @property string $priority
 * @property string $parent
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $update_at
 * @property string $deleted_at
 * @property int $trees_id
 * 
 * @property \App\Models\Tree $tree
 *
 * @package App\Models
 */
class Queue extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id' => 'int',
		'trees_id' => 'int'
	];

	protected $dates = [
		'update_at'
	];

	protected $fillable = [
		'nama',
		'ipaddress',
		'pupload',
		'mupload',
		'pdownload',
		'mdownload',
		'priority',
		'parent',
		'update_at',
		'trees_id'
	];

	public function tree()
	{
		return $this->belongsTo(\App\Models\Tree::class, 'trees_id');
	}
}
