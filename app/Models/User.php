<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 19 Jan 2020 17:24:04 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class User
 * 
 * @property int $id
 * @property string $nama
 * @property string $email
 * @property string $password
 * @property string $company
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $aktif
 * 
 * @property \Illuminate\Database\Eloquent\Collection $routers
 *
 * @package App\Models
 */
class User extends Eloquent
{
	protected $casts = [
		'aktif' => 'int'
	];

	protected $hidden = [
		'password'
	];

	protected $fillable = [
		'nama',
		'email',
		'password',
		'company',
		'aktif'
	];

	public function routers()
	{
		return $this->hasMany(\App\Models\Router::class, 'users_id');
	}
}
