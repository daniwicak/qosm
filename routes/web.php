<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['prefix' => '/'], function () {
    Route::get('/', function () {
        return view("default.menus.home.index");
        // return view("default.layouts.default");
    });
    Route::get('register', 'Defaults\RegisterController@index');
    Route::post('register', 'Defaults\RegisterController@post');
    Route::get('login', 'Defaults\LoginController@index')->name('login');
    Route::post('login', 'Defaults\LoginController@login');
});

Route::group(['middleware' => 'auth', 'prefix' => 'tree'], function () {
    Route::get('/', 'Tree\TreeController@index');
    Route::get('router', 'Tree\RouterController@index');
    Route::get('router/add', 'Tree\RouterController@indexDetail');
    Route::post('router/add', 'Tree\RouterController@postDetail');
    Route::get('router/del', 'Tree\RouterController@delete');
    Route::get('trees', 'Tree\TreesController@index');
    Route::get('trees/add', 'Tree\TreesController@indexDetail');
    Route::post('trees/add', 'Tree\TreesController@postDetail');
    Route::get('trees/del', 'Tree\TreesController@delete');
    Route::get('logout', 'Defaults\LoginController@logout');
});
